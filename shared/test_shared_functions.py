"""
Title: test_shared_functions.py
Date: 16/10/2023
Author: Alberto Fabbri

Description:
    This module contains a series of function used to unittest the functions inside
    the shared_function module

    This program opens different types of files with various types of errors and
    check if the validate_fasta function returns the correct list of errors.
"""

import pathlib
import shared_functions
import unittest

THIS_DIR = pathlib.Path(__file__).parent

class TestSharedFunctions(unittest.TestCase):

    def test_fasta_normal(self):
        with open(THIS_DIR / "test_files" / "score.fna") as score:
            errors = shared_functions.validate_fasta(score, "ACGT-")
            self.assertEqual(errors, {})

    def test_fasta_starts_with_sequence(self):
        with open(THIS_DIR / "test_files" / "score.extra.start.with.sequence.fna") as score:
            errors = shared_functions.validate_fasta(score, "ACGT-")
            self.assertEqual(errors, {1:[3]})

    # This test succeed only because if the fasta file starts with a sequence,
    # the first sequence is not checked against a regex, see share_functions.py known bugs
    def test_fasta_starts_with_invalid_sequence(self):
        with open(THIS_DIR / "test_files" / "score.extra.start.with.invalid.sequence.fna") as score:
            errors = shared_functions.validate_fasta(score, "ACGT-")
            self.assertEqual(errors, {1:[3]})

    def test_fasta_ends_with_id(self):
        with open(THIS_DIR / "test_files" / "score.extra.ends.with.id.fna") as score:
            errors = shared_functions.validate_fasta(score, "ACGT-")
            self.assertEqual(errors, {-1:[5]})
    
    def test_fasta_double_header(self):
        with open(THIS_DIR / "test_files" / "score.extra.double.header.fna") as score:
            errors = shared_functions.validate_fasta(score, "ACGT-")
            self.assertEqual(errors, {16:[1]})
    
    def test_fasta_sequences_different_length(self):
        with open(THIS_DIR / "test_files" / "score.extra.seq.different.length.fna") as score:
            errors = shared_functions.validate_fasta(score, "ACGT-", True)
            self.assertEqual(errors, {12:[6], 17:[6], 22:[6]})

    def test_fasta_invalid_sequence(self):
        with open(THIS_DIR / "test_files" / "score.extra.invalid.sequence.fna") as score:
            errors = shared_functions.validate_fasta(score, "ACGT-")
            self.assertEqual(errors, {17:[2]})

    def test_fasta_multiple_errors(self):
        with open(THIS_DIR / "test_files" / "score.extra.multiple.errors.fna") as score:
            errors = shared_functions.validate_fasta(score, "ACGT-", True)
            self.assertEqual(errors, {11:[1], 13:[2,6], 23:[6], 28:[2], -1:[5]})

# run the test
unittest.main()
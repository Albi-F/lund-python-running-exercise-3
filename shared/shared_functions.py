#!/usr/bin/env python3

"""
Title: shared_functions.py
Date: 23/10/2023
Author: Alberto Fabbri

Description:
    This module contains some generic functions that can be imported by other scripts.

    This program can be execute standalone to check as much as possible the correctness of a fasta file,
    it will check the fasta file provided as the first argument on the cmd
    with the characters expected in the sequences specified by the second optional parameter.
    A third optional parameter can be used to check if the sequences are all of the same length.

List of functions:
    validate_fasta()

Usage:
    python shared_functions.py input_fasta.fna (default: "ACGTacgt") [optional] (True|False) [optional]
"""

import collections
import itertools
import re
import sys

# Make it global so it is accessible from other scripts that wants to print the list of errors received
f_error_dictionary :dict[int, str] = {
    1 : "multiple headers",
    2 : "invalid nucleotide sequence",
    3 : "should be a header",
    4 : "should be a sequence",
    5 : "file should end with a sequence",
    6 : "sequence have different length than previous one"
}

def validate_fasta(fasta_file :list[str], f_seq_characters :str = "ACGTacgt", f_equal_length :bool = False):
    """
    This function takes a fasta file and check if it is syntactically correct.

    Errors checked:
    - headers in consecutive rows
    - invalid nucleotide sequences
    - that the file starts with a header
    - that the file ends with a sequence
    - [Optional] that the sequences have the same length

    Known bugs:
    - In case the fasta file starts with a sequence, the first sequence
    is not checked against the regex

    Input:
    1. [Mandatory] the fasta file to be checked as a list of strings.
       Do not pass it as a TextIOWrapper because in that case the iterator is fully
       used by this function and can not be used in your script anymore (unless you reopen the file)
    2. [Optional, Default: "ACGTacgt"] the characters expected to be part of the sequences
       This is used to construct the regex used to check the validity of the sequences.
    3. [Optional, Default: False] a boolean that specify if the sequences should all be of the same length
    
    Output:
    1. A dictionary with all the errors, the keys are the lines in which the errors are found,
      the values are lists with all the errors found on that group (see the f_error_dictionary)
    """

    # Create the regex from the parameter, escape eventual special regex characters
    regex = f"[{re.escape(f_seq_characters)}]+"

    # Dictionary for saving the errors, if returned empty by the function
    # it means that no errors have been found
    f_errors = collections.defaultdict(list)

    # Construct a generator that can iterate over the fasta file one group at a time
    # Groups are made of lines that start with an ">" (headers) or do not (sequences)
    f_iterable = ((f_key, list(f_group)) for (f_key, f_group) in itertools.groupby(fasta_file, lambda line: line[0] == ">"))

    # Initialize variables to keep track of the line where the header and the sequence starts
    f_group1_rows :int = 0
    f_group2_rows :int = 0
    f_group1_start_line :int = 1
    f_group2_start_line :int = 1

    try:
        # Iterate over the fasta file two groups at a time (a header in group1 + a sequence in group2)
        for (f_g1_is_header, f_group1), (f_g2_is_header, f_group2) in zip(f_iterable, f_iterable, strict=True):

            # Calculate the line at which the header and the sequence start
            # Using enumerate works only if the groups are always one line
            f_group1_start_line += f_group1_rows + f_group2_rows
            f_group1_rows = "".join(f_group1).count("\n")
            f_group2_start_line += f_group2_rows + f_group1_rows
            
            # Checks that group 1 is a header
            if not f_g1_is_header:
                f_errors[f_group1_start_line].append(3)
                # If the file starts with a sequence it is likely that all headers and sequence are inverted
                # Therefore the program would throw an error for every new group
                # To avoid this, if this error is detected, the iterator is increased only half
                f_iterable.__next__()
                continue
                
            # Checks that group 2 is a sequence
            if f_g2_is_header:
                f_errors[f_group2_start_line].append(4)
                f_iterable.__next__()
                continue

            # Checks that the header group contains only 1 header
            if len(f_group1) > 1:
                f_errors[f_group1_start_line].append(1)

            # Checks if the nucleotide sequence is valid
            if not re.fullmatch(regex,f_seq := ''.join(map(str.strip, f_group2))):
                f_errors[f_group2_start_line].append(2)
            
            # Checks if sequences have all the same length
            # It compares the current sequence against the previous one
            if f_equal_length:
                if f_group2_rows != 0 and prev_seq_len != len(f_seq):
                    f_errors[f_group2_start_line].append(6)
                prev_seq_len = len(f_seq)

            # Update the rows of group 2 here so I can use this variable to check if it is the first loop
            f_group2_rows = "".join(f_group2).count("\n")

    # Throw this error if the fasta file ends with a header
    # Thanks to the strict option of zip
    except ValueError:
        f_errors[-1].append(5)
    
    # Returns the dictionary with the found errors
    return f_errors


# Use if you want to check a fasta file
def main():
    # Open the fasta file provided on the command line
    with open(sys.argv[1]) as fasta_file:

        # SEQUENCE TYPE
        # Sets the optional parameter sequence type to the default
        seq_type = validate_fasta.__defaults__[0]
        # Overwrite the optional parameter sequence type default if a value has been given on the command line
        if len(sys.argv) >= 3: seq_type = sys.argv[2]

        # EQUAL LENGTH
        # Sets the optional parameter equal length to the default
        equal_length = validate_fasta.__defaults__[1]
        # Overwrite the optional parameter equal sequence default if a value has been given on the command line
        if len(sys.argv) >= 4: equal_length = bool(sys.argv[3])

        # Calls the validate fasta function and save the returned value
        errors_found = validate_fasta(fasta_file, seq_type, equal_length)

        # Prints the errors found
        for f_err_line in errors_found.keys():
            print(f"line {f_err_line}")
            for f_error in errors_found[f_err_line]:
                print(f"\tError: {f_error_dictionary[f_error]}")
        if not errors_found:
            print("No errors found")

# Execute when run from command line
if __name__ == "__main__":
    main()

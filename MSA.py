#!/usr/bin/env python3

"""
Title: MSA.py
Date: 23/10/2023
Author: Alberto Fabbri

Description:
    This script calculates how well two DNA sequences align with each other.

    This program will print and optionally output a file with some statistics
    (score, percentage identity, percentage gaps)
    about the alignment of every possible combination of two sequences.

    The user must specify the fasta file with the aligned sequences and optionally
    a file with different parameters used to calculate the alignment score and
    a name for the output file.

Procedure:
    1. Create a dictionary with all the sequences from the fasta file.
    2. Iterate over all the combinations of sequences
    3. Iterate over the two sequences one base at a time and calculate the gaps, identity and score

Input:
    1. [Mandatory] the fasta file with the aligned sequences
    2. [Optional, Default: see the parameters dictionary] a parameter file formatted as shown below
    3. [Optional] the name of the output file
    
Output:
    1. It prints and optionally creates a file with the
    alignment identity, gaps and score for every combination of two sequences.
    Example:
        id1-id2: Identity: 145/205 (70.73%), Gaps 18/205 (8.78%), Score=82

The parameter file:
    The parameters inside this file should be called "match", "transition", "transversion", "gap" and "unknown".
    Each parameter should be on a new line with its value separated by a space.
    The order of the parameters does not matter.
    In case unknown parameters are encountered an error will be thrown
    (a line with only whitespace or other special characters is going to be considered and unknown parameter).
    Example:
        match +7
        transition -2
        transversion -4
        gap -2
        unknown -1

Usage:
    python FastaAligner.py input_fasta.fna --parameters parameters.txt --output result.txt

Syntax used:
p_ for variables related to the parameter file
f_ for variables related to the fasta file
o_ for variables related to the output
"""

import argparse
import itertools
import re
import shared.shared_functions as shared_functions
import sys

# Create a parser and add some text that is printed if the user launch with the help flag
parser = argparse.ArgumentParser(prog= "MSA",
                                 description="Calculate the alignment score of sequences in a fasta file",
                                 epilog="Thank you for using the Multiple Sequence Aligner",
                                 allow_abbrev=False)

# Add the expected arguments, the mandatory input file and the optionals parameter and output files
parser.add_argument("fasta_file")
parser.add_argument("-p", "--parameter")
parser.add_argument("-o", "--output", default="")

# Save the returned values
args = parser.parse_args()

# Default values
parameters = {
    "match" : +1,
    "transition" : -1,
    "transversion" : -2,
    "gap" : -1,
    "unknown": 0
}

# Symbols in nucleotides sequences
symbols = {
    "gap": "-",
    "unknown": "?"
}

transitions = {
    frozenset(["A", "G"]),
    frozenset(["C", "T"])
}

transversions = {
    frozenset(["A", "C"]),
    frozenset(["A", "T"]),
    frozenset(["G", "C"]),
    frozenset(["G", "T"])
}

# If a parameters file is provided override the defaults
if (parameter_file := args.parameter):
    try:
        with open(parameter_file, 'r') as parameter_file:
            #Read line by line keeping track of the current line with a counter
            for p_line_num, p_line_string in enumerate(parameter_file, start=1):
                # Split every line into parameter and corresponding value
                # If the expected syntax is not followed this will raise a ValueError exception
                p_parameter, p_value = p_line_string.split()
                # Override the value in the parameters dictionary if is a valid parameter
                if p_parameter in parameters:
                    parameters[p_parameter] = int(p_value)
                # Raise an error otherwise
                else:
                    raise ValueError
    # Print an error if the file does not exists
    except IOError as p:
        sys.exit(f'{p.strerror}: {p.filename}')
    # Print an error and the corresponding line if the expected syntax is not followed
    except ValueError as v:
        sys.exit(f"Parameter file syntax is invalid, check line {p_line_num}")

# String to be printed out
o_lines = ""

try:
    with open(args.fasta_file, 'r') as fasta_file:

        # # This way it is possible to iterate over the file more than once
        fasta = fasta_file.readlines()

        # # Call a function on an external module to check if the fasta file is correct
        f_errors = shared_functions.validate_fasta(fasta, "ACGT-?", True)

        # # If errors have been found, print them and exit
        for f_err_line, f_error in f_errors.items():
            print(f"Error: {shared_functions.f_error_dictionary[f_error]}, line {f_err_line}")
            sys.exit()

        # The key is the id, the value is the nucleotide sequence
        f_sequences :dict[str, str] = {}

        # Construct a generator that can iterate over the fasta file one group at a time
        # Groups are made of lines that start with an ">" (headers) or do not (sequences)
        f_iterable = ((key, list(group)) for (key, group) in itertools.groupby(fasta, lambda line: line[0] == ">"))

        # Iterate over the fasta file two groups at a time (a header in group1 + a sequence in group2)
        for (f_g1_is_header, f_group1), (f_g2_is_header, f_group2) in zip(f_iterable, f_iterable, strict=True):

            # Use a regex to extract the sequence id from the header
            f_seq_id :str = re.search(r"^>\s*([a-zA-Z0-9_'’]+)\b", (f_header := ''.join(f_group1).rstrip())).group(1)

            # Save id and sequence to the dictionary
            f_sequences[f_seq_id] = ''.join(map(str.strip, f_group2)).upper()

        # Iterate over all the possible combination of sequences
        for ((id1, seq1), (id2, seq2)) in itertools.combinations(f_sequences.items(), 2):

            # Reset the statistics for every new combination
            identity = gaps = score = 0
            # Save the length of the sequences
            seq_len = len(seq1)

            # Iterate over two sequences (strings) at a time
            # Enumerate is used to keep track of the current position
            for base_pos, base_pair in enumerate(zip((seq1), seq2)):

                # Add the value of match to the score if the base is the same in
                # both sequences, but not if both have a gap
                # Add 1 to the identity counter
                if base_pair[0] == base_pair[1] != symbols["gap"]:
                    identity += 1
                    score += parameters["match"]

                # If there is at least one unknown nucleotide in one of the sequences
                elif symbols["unknown"] in base_pair:
                    score += parameters["unknown"]

                # If there is at least one gap in one of the sequences
                elif symbols["gap"] in (base_pair):
                    # If the gap is present on only one sequence add the gap value to
                    # the score and add 1 to the gaps counter
                    if base_pair[0] != base_pair[1]:
                        gaps += 1
                        score += parameters["gap"]
                    # Else do nothing
                    # The comment out line is in case the gap should not be counted in the length
                    # if both sequences have it
                    else:
                        # seq_len -= 1
                        pass
                
                # Look in the transition set if the mutation is a transition
                # There is no need to check the inverted order as this is a set
                elif frozenset(base_pair) in transitions:
                    score += parameters["transition"]

                # Look in the transversion set if the mutation is a transversion
                elif frozenset(base_pair) in transversions:
                    score += parameters["transversion"]
            
            # Calculate the needed statistics
            identity_percentage = identity/seq_len
            gaps_percentage = gaps/seq_len

            # Construct the output string
            o_lines += (f"{id1} {id2} {identity_percentage*100:.2f}% {score}\n")

# Throw an error if the input file does not exist or the output file is going to be overwritten
except IOError as e:
    sys.exit(f'{e.strerror}: {e.filename}')

# Print the output
o_header = "SampleA SampleB IdentityScore Score"
o_text = f"{o_header}\n{o_lines}"

# PRINT THE OUTPUT
# on the standard output
print(o_text)
# on a file if a filename has been provided
if (output_filename := args.output):
    try:
        with open(output_filename, 'x') as output_file:
            output_file.writelines(o_text)
    except IOError as e:
        sys.exit(f'{e.strerror}: {e.filename}')

#!/usr/bin/env python3

"""
Title: FastaParser.py
Date: 23/10/2023
Author: Alberto Fabbri

Description:
    This script reads a txt file and parses it to creates two fasta file
    with different genetic data.

    The two fasta files produced contains y chromosome and mitochondrial DNA data.

    The user must specify the fasta file with the aligned sequences and optionally
    a prefix to be used to name the output files.

Procedure:
    1. Parse the text file line by line.
    2. Save the first line after a blank line as the id
    3. Look for a sequence header, when found save id and sequence to the corresponding fasta file
    4. Repeat steps 2 and 3 for every block of data (person)

Input:
    1. [Mandatory] the text file with the sequences
    2. --output [Optional, Default: "output"] the prefix to be used for the fasta files

Output:
    It creates two fasta with the y chromosome and mitochondrial dna sequences.

Usage:
    python FastaParser.py input_fasta.fna --output sequences

Syntax used:
i_ for variables related to the input text file
f_ for variables related to the output fasta files
"""

import argparse
import re
import sys

# Create a parser and add some text that is printed if the user launch with the help flag
parser = argparse.ArgumentParser(prog= "FastaParser",
                                 description="Split the mitochondrial dna and y chromosome sequences from a text file",
                                 epilog="Thank you for using the FastaParser",
                                 allow_abbrev=False)

# Add the expected arguments, the mandatory input file and the optionals parameter and output files
parser.add_argument("input_file")
parser.add_argument("-o", "--output", default="output")

# Save the returned values
args = parser.parse_args()

# Set the headers to recognize the different sequences
mtDNA_header :str = "mtDNA"
y_chr_header :str = "Y chromosome"

def main():
    try:
        # Open all the files at the same time so if there is an error we can catch it immediately
        with open(args.input_file, mode='r', encoding='windows-1252') as input_file,\
             open(f"{args.output}_mtDNA.fna", 'x') as fasta_mtDNA,\
             open(f"{args.output}_y_chr.fna", 'x') as fasta_y_chr:

            # Variables used to iterate over the text file
            # Save the current person name and set a flag when encountering a sequence header
            f_header: str = ""
            y_chr_on_next_line = False
            mtDNA_on_next_line = False
            
            # Iterate over the text file one line at a time
            for i_line in input_file:

                # Remove the greater than sign and eventual spaces at the beggining
                # and end of the line
                i_line = re.sub(">", "", i_line.strip())
                
                # Set a flag if the line is equal to the mtDNA header
                if i_line == mtDNA_header:
                    mtDNA_on_next_line = True
                
                # If the mtDNA flag was set on the previous loop: read the sequence,
                # save it to the appropriate fasta file together with the header and
                # reset the flag
                elif mtDNA_on_next_line:
                    fasta_mtDNA.write(f"{f_header}\n{i_line}\n")
                    mtDNA_on_next_line = False
                
                # Set a flag if the line is equal to the y chromosome header
                elif i_line == y_chr_header:
                    y_chr_on_next_line = True

                # If the y_chr flag was set on the previous loop: read the sequence,
                # save it to the appropriate fasta file together with the header and
                # reset the flag
                elif y_chr_on_next_line:
                    fasta_y_chr.write(f"{f_header}\n{i_line}\n")
                    y_chr_on_next_line = False

                # If the previous line was empty and the fasta header variable is empty,
                # then this line should be a header
                elif i_line != mtDNA_header and i_line != y_chr_header and i_line != "" and f_header == "":
                    f_header = f">{i_line.replace(' ', '_')}"

                # Reset the fasta header on an empty line
                elif i_line == "":
                    f_header = ""

    # Throw an error if a file do not exist or is going to be overwritten
    except IOError as e:
        sys.exit(f'{e.strerror}: {e.filename}')

# Do not execute it when imported
if __name__ == "__main__":
    main()

Princess Irene 
mtDNA
AGGGAT??CTAGCTAGCA?AGCTAAT

Prince Fred 
mtDNA
ACC??TAGCTAG?TAGCATAGCTAAT
Y chromosome
AGCCTAGCTCGCGCGCGCATATAGCTAGACTAGCATCGAGAGAGAGAG

Nicolas II Romanov 
Not a hemophilia patient
mtDNA
-----GGGG-----AAA---------
Y chromosome
AAAA---GAT-CCC-AACA-CGGG-----CGC-GGGGG---CGT----

Alexandra Romanov 
A hemophilia carrier
mtDNA
AC---A----A---AGCA---?TA?C

Olga Romanov 
Not a hemophilia patient
mtDNA
AGGG???---AGCTA--ATAG?TAGC

Tatiana Romanov 
Not a hemophilia carrier
mtDNA
AGG-------A------------AGC

Maria Romanov 
Not a hemophilia carrier
mtDNA
ACC???AGCTAGCT----TAGCTTTT

Alexei Romanov 
A hemophilia patient
mtDNA
ACC--TAGCT???TAGCAT?GCT---
Y chromosome
AAAA-----------AAAA--------------GGGGG-----TTT--

Suspected body of Anastasia Romanov 
A hemophilia carrier
mtDNA
??CAGAGACTCTC???CAAAGCTAGC


Anastasia1
Not a hemophilia carrier
mtDNA
ACTGTACGCCA??CAAGCA?G??A?C

Anastasia2
A hemophilia carrier
mtDNA
AGGG???GCT???GG????AGGGGAC

Anastasia3
Not a hemophilia carrier
mtDNA
ACC???AGC????GAGCATA??AATT

Anastasia4
A hemophilia carrier

Anastasia4 son
A hemophilia patient
mtDNA
ACCAA??GCTAA?TAGCAT-GCT--G
Y chromosome
AGCGCATATACGATCGATCAGCTGCATCGACTAGCATCGATACGATCG

Anastasia5
Not a hemophilia carrier
mtDNA
???AGAGACT??CTC??AAAGCT---

Farmer's daughter
Not a hemophilia carrier

>Farmer�s grandson
Not a hemophilia patient
mtDNA
AACAGAGACTAAA???CAAAGCTATT
Y chromosome
??ATATATACGCGCTAC??TCAGCATAGGTATAGCGCGCTAAGCATCG

Grigori Rasputin
Not a hemophilia carrier
mtDNA
AAAAAGGGGGGGGGCCCCCTATATAT
Y chromosome
?????TATACGCGAAACGATCAGCATGGGTATAGCGCGCTAA?CAT??
